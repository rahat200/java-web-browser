/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javabrowser;

import java.awt.*;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import javax.swing.plaf.basic.BasicSplitPaneUI;

/**
 *
 * @author user
 */
public class BrowserWindow implements Runnable,ActionListener{
    
    JFrame frame;
    String url;
    Thread t;
    JButton tab1,tab2;
    Browse b1,b2;
    public BrowserWindow(String url)
    {
        this.url=url;
        
        t=new Thread(this, url);
        System.out.println("new thread:"+t);
        t.start();
         
    }

    @Override
    public void run() {
        try {
             
            frame=new JFrame("#Browser#");
            
            //test
            JPanel tabpanel=new JPanel(new BorderLayout());
            JScrollPane tabcont=new JScrollPane(tabpanel);
            tabpanel.setLayout(new BoxLayout(tabpanel, BoxLayout.X_AXIS));
            tab1=new JButton("tab1");
            tab2=new JButton("tab2");
            tab1.addActionListener(this);
            tab2.addActionListener(this);
            tabpanel.add(tab1);
            tabpanel.add(tab2);
            frame.getContentPane().add(tabcont);
            //test
            
            
             //b1=new Browse(url,frame);
            // b2=new Browse("I:/CSE-BUET/WEB stuffs/prachtml.html",frame);
            //b1.t.stop();
            
           
            
            frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.X_AXIS));//changed later
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setLocation(300, 30);
            frame.setSize(800, 600);
            frame.setVisible(true);
            
            Thread.sleep(10000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==tab1)
        {
             
             if(b2!=null && b2.t.isAlive())
             {
                 b2.t.stop();
                 b2.Invisible();
             }
            b1=new Browse(url,frame);
             b1.t.start();

        }
        if(e.getSource()==tab2)
        {
                  if(b1!=null&&b1.t.isAlive())
                  {
                    b1.t.stop();
                    b1.Invisible();
                  }
            b2=new Browse("I:/CSE-BUET/WEB stuffs/prachtml.html",frame);
             b2.t.start();

        }
    }
}
