/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javabrowser;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Tamjid Al Rahat
 */
public class Browse implements Runnable{
    public JPanel panel;
    public JFrame frame;
    //public JScrollPane container;
    public String url;
    Thread t;
    
    public Browse(String url,JFrame frame)
    {
        this.url=url;
        this.frame=frame;
        t=new Thread(this, url);
        //t.start();
    }
        @Override
    public void run() {
        try {
            System.out.println(url+"===");
            panel=new JPanel();
            JScrollPane container=new JScrollPane(panel);
            frame.getContentPane().add(container);
            panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
            panel.setBackground(Color.WHITE);
            panel.setMaximumSize(new Dimension(800, 600));
            Connect(this.url);
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
    public void Invisible()
    {
        if(panel!=null)
            panel.setVisible(false);
    }
    
//connect ################################################//
    
    public void Connect(String url)
    {
        org.jsoup.nodes.Document doc=new Document(url);
        if ((url.toLowerCase().startsWith("http://")))
        {
            try {
                //org.jsoup.nodes.Document doc=new Document(url);
                    doc =Jsoup.connect(url).get();
                    ParseAllElements(doc);
                     
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        else{
            File input=new File(url);
            try {
                doc = Jsoup.parse(input, "UTF-8");
                ParseAllElements(doc);
            } catch (IOException ex) {
                System.out.println("file cant open");
                ex.printStackTrace();
            }
        }
    }
    
//start parsing##########################################//    
    public void ParseAllElements(org.jsoup.nodes.Document doc)
    {
    
        Elements content = doc.getAllElements();
        Elements bodycontent=null;

        for(Element tag:content){

            if(tag.tagName()=="body")
            {
                    bodycontent=tag.children();
            }
        }
        if(bodycontent!=null){
            for (Element tag : bodycontent)
            {
                if(tag.tagName()=="h4")
                {
                    Parse_tag_h_tag(panel,tag);
                }
                else if(tag.tagName()=="p")
                {
                    Parse_tag_p(panel,tag);
                }
                else if(tag.tagName()=="a")
                {
                    Parse_tag_a( tag);
                }
                else if(tag.tagName()=="table")
                {
                    Parse_tag_table(panel,tag);
                }
                else if(tag.tagName()=="img")
                {
                    Parse_tag_img(panel, tag, url);
                }
                else if(tag.tagName()=="form")
                {
                    Parse_tag_form(panel, tag);
                }

            }
        }    
    }
    
    public void Parse_tag_p(JPanel panel,org.jsoup.nodes.Element tag)
    {

        JEditorPane dispane=new JEditorPane();
        dispane.setEditable(false);
        dispane.setEditorKit(new HTMLEditorKit());
        HTMLDocument htmldoc= (HTMLDocument) dispane.getDocument();
        dispane.setLayout(new BorderLayout());
        dispane.setAlignmentX(Component.LEFT_ALIGNMENT);
                try{

                    String p_text=tag.text();
                    SimpleAttributeSet attrs = new SimpleAttributeSet();
                    StyleConstants.setFontSize(attrs, 14);//default font size
                    if(tag.children()!=null)
                    {
                        for(Element attr_tag:tag.children())
                        {
                            if(attr_tag.tagName()=="b")
                            {
                                StyleConstants.setBold(attrs, true);
                                htmldoc.insertString(htmldoc.getLength(),attr_tag.text(), attrs);
                                p_text=p_text.replaceAll(attr_tag.text(), "");
                            }
                            if(attr_tag.tagName()=="i")
                            {
                                StyleConstants.setItalic(attrs, true);
                                htmldoc.insertString(htmldoc.getLength(),attr_tag.text(), attrs);
                                p_text=p_text.replaceAll(attr_tag.text(), "");
                            }
                        }
                    }
                        StyleConstants.setBold(attrs, false);
                        StyleConstants.setItalic(attrs, false);
                        htmldoc.insertString(htmldoc.getLength(),p_text+"\n", attrs);
                        }
                        catch (BadLocationException ex) {
                                ex.printStackTrace();
                    }
                    Dimension size=new Dimension(800, dispane.getPreferredSize().height);
                    dispane.setMaximumSize(size); 

                    panel.add(dispane, BorderLayout.WEST);
        if(tag.children().size()!=0)
        {
            Elements child=tag.children();
            for(Element each_child:child)
            {
                if(each_child.tagName()=="a")
                {
                    Parse_tag_a( each_child);
                }
            }        
        }                

    }
    
    public void Parse_tag_table(JPanel panel,org.jsoup.nodes.Element tag) 
    {
        try{
        Elements tblchild=tag.children();

        Elements rows= tblchild.select("tr");
        int row_num=rows.size();

        int col_num=rows.first().children().size();

        boolean have_th=rows.first().select("th").size()==col_num?true:false;
        if(have_th==true) 
            row_num=row_num-1;//header containing row is not included in table row,but only in header[]

        Object data[][]=new Object[row_num][col_num];
        String header[]=new String[col_num];

        int h=0,i=0,j=0;
        for(Element row:rows)
        {
            j=0;
            Elements row_data=row.children();
            if(row_data.select("th").size()==row_data.size())
            {
                for(Element col_data:row_data)
                {
                    if(col_data.tagName()=="th")
                        header[h++]=col_data.text();
                }
            }
            if(row_data.select("td").size()==row_data.size())
            {
                j=0;
                for(Element col_data:row_data)
                {
                    if(col_data.tagName()=="td")
                    {
                        Elements td_child=col_data.children();

                        if(td_child.size()!=0){
                            for(Element each_child:td_child)
                            {
                                //System.out.println(each_child.tagName());
                                if(each_child.tagName()=="a")
                                    Parse_tag_a( each_child);
                                //Parse_tag_a(panel, each_child);
                            }
                        }
                        else
                            data[i][j++]=col_data.text();
                    }
                }
                i++;
            }

        }

            if(have_th==false)
        {
            for(i=0;i<col_num;i++)
                header[i]=" ";
        }

        JTable table=new JTable(data,header);

        if(have_th==false)
        {
            table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            table.setFillsViewportHeight(true);
            table.setPreferredScrollableViewportSize(table.getPreferredSize());
            table.setTableHeader(null);
            table.setCellSelectionEnabled(false);

            JScrollPane scrollpane=new JScrollPane(table);
            scrollpane.setAlignmentX(Component.LEFT_ALIGNMENT);
            scrollpane.setMaximumSize(scrollpane.getPreferredSize());
            scrollpane.getVerticalScrollBar().setPreferredSize (new Dimension(0,0));
            scrollpane.getHorizontalScrollBar().setPreferredSize (new Dimension(0,0));
            panel.add(scrollpane,BorderLayout.CENTER);
        }
        else{
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.setRowHeight(20);
        table.setPreferredSize(new Dimension(table.getColumnModel().getTotalColumnWidth(),(table.getRowCount()+1)*20 ));
        table.setPreferredScrollableViewportSize(table.getPreferredSize());
        table.setFillsViewportHeight(false);
        table.setCellSelectionEnabled(false);

        JScrollPane scrollpane=new JScrollPane(table);

        scrollpane.setAlignmentX(Component.LEFT_ALIGNMENT);
        scrollpane.setMaximumSize(scrollpane.getPreferredSize());
        scrollpane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollpane.getVerticalScrollBar().setPreferredSize (new Dimension(0,0));
        scrollpane.getHorizontalScrollBar().setPreferredSize (new Dimension(0,0));
        panel.add(scrollpane,BorderLayout.WEST);

        }    if(tag.children().first().tagName()=="caption")
            Parse_tag_p(panel,tag.children().first()); 
        }catch(Exception ex){
            System.out.println("invalid table");
        }

    }
    
    public void Parse_tag_img(JPanel panel,org.jsoup.nodes.Element tag,String url)
    {
        String img_dir=tag.attr("src");
        if(img_dir.toLowerCase().startsWith("http://"))
        {
        try {
            URL img_url=new URL(img_dir);

            ImageIcon icon=new ImageIcon(img_url);

            JLabel label = new JLabel("", icon, JLabel.CENTER);
            panel.add( label, BorderLayout.CENTER );
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        }
        }
        else{
            String img_url=url.substring(0, url.lastIndexOf("/")+1)+img_dir;
            ImageIcon image;
            if(img_url.toLowerCase().startsWith("http://"))
            {
                try {
                    image = new ImageIcon(new URL(img_url));
                    JLabel label = new JLabel("", image, JLabel.CENTER);
                    panel.add( label, BorderLayout.CENTER );
                } catch (MalformedURLException ex) {
                    ex.printStackTrace();
                }
            }
            else{
                    image = new ImageIcon((img_url));
                    JLabel label = new JLabel("", image, JLabel.CENTER);
                    panel.add( label, BorderLayout.CENTER );
            }

        }
    }
    
    public void Parse_tag_h_tag(JPanel panel,org.jsoup.nodes.Element tag)
    {
        JEditorPane dispane=new JEditorPane();
        dispane.setEditable(false);
        dispane.setEditorKit(new HTMLEditorKit());
        HTMLDocument htmldoc= (HTMLDocument) dispane.getDocument();
        dispane.setLayout(new BorderLayout());
        dispane.setAlignmentX(Component.LEFT_ALIGNMENT);

        SimpleAttributeSet attrs = new SimpleAttributeSet();
        StyleConstants.setBold(attrs, true);
        StyleConstants.setFontSize(attrs, 15);
                try {
                    htmldoc.insertString(htmldoc.getLength(),tag.text()+"\n", attrs);
                } catch (BadLocationException ex) {
                    ex.printStackTrace();
                }

        Dimension size=new Dimension(800, dispane.getPreferredSize().height);
        dispane.setMaximumSize(size); 

        panel.add(dispane, BorderLayout.WEST);

    }
    
    public void Parse_tag_a(org.jsoup.nodes.Element tag)
    {
        if(tag.children().size()!=0)
        {
            Elements child=tag.children();
            for(Element each_child:child)
            {
                if(each_child.tagName()=="img")
                {
                    Parse_tag_img(panel, each_child,url);
                }
            }        
        }  

        final JEditorPane dispane=new JEditorPane();
        dispane.setEditable(false);
        dispane.setEditorKit(new HTMLEditorKit());
        final HTMLDocument htmldoc= (HTMLDocument) dispane.getDocument();
        dispane.setLayout(new BorderLayout());
        dispane.setAlignmentX(Component.LEFT_ALIGNMENT); 

        String tagurl=tag.attr("href");
        final String tagtext=tag.text();
        SimpleAttributeSet attrs = new SimpleAttributeSet();
        StyleConstants.setUnderline(attrs, true);
        StyleConstants.setForeground(attrs, Color.blue);
        StyleConstants.setFontSize(attrs, 14);//default font size

        attrs.addAttribute(HTML.Attribute.HREF, tagurl);
    try {
        htmldoc.insertString(htmldoc.getLength(),tag.text()+"\n", attrs);

    } catch (BadLocationException ex) {
        ex.printStackTrace();
    }
        dispane.addMouseMotionListener(new MouseMotionListener() {

            @Override
            public void mouseDragged(MouseEvent e) {

            }

            @Override
            public void mouseMoved(MouseEvent e) {
                //System.out.println("mouse is being moved");
                Point pt=new Point(e.getX(), e.getY());
                int pos=dispane.viewToModel(pt);
                javax.swing.text.Element elem=htmldoc.getCharacterElement(pos);
                AttributeSet atset=elem.getAttributes();

                String href = (String) atset.getAttribute(HTML.Attribute.HREF);
                if(href!=null){
                    if(dispane.getCursor()!=new Cursor(Cursor.HAND_CURSOR))
                    {
                        dispane.setCursor(new Cursor(Cursor.HAND_CURSOR));
                        dispane.setToolTipText(tagtext);
                    }
                }
                else
                {
                    dispane.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                }
            }
        });    

        dispane.addMouseListener(new MouseListener() {

        @Override
        public void mouseClicked(MouseEvent e) {
            Point pt=new Point(e.getX(), e.getY());
            int pos=dispane.viewToModel(pt);
            javax.swing.text.Element elem=htmldoc.getCharacterElement(pos);
            AttributeSet atset=elem.getAttributes();

            String href = (String) atset.getAttribute(HTML.Attribute.HREF);
            if(href!=null){
                Clear_Container();
                new Browse(href,frame);
                System.out.println(href);
            }
        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }
    });                    

        Dimension size=new Dimension(800, dispane.getPreferredSize().height);
        dispane.setMaximumSize(size); 

        panel.add(dispane, BorderLayout.WEST);                    

    }

    public void Parse_tag_form(JPanel panel,org.jsoup.nodes.Element tag)
    {
        StringTokenizer st=new StringTokenizer(tag.text(), " ");
        if(tag.children().size()!=0)
        {
            Elements childs=tag.children();
            int txtfield_num=0;
            for(Element each_child:childs)
            {
                if(each_child.tagName().equals("input")&&each_child.attr("type").equals("text"))
                    txtfield_num++;
            }
            JTextField txtfield[]=new JTextField[txtfield_num];
            int i=0;
            for(Element each_child:childs)
            {
                if(each_child.tagName().equals("input"))
                {
                    String type=each_child.attr("type");
                    if(type.equals("text"))
                    {
                        txtfield[i]=new JTextField();
                        JLabel label=new JLabel();
                        label.setText(st.nextToken());
                        txtfield[i].setMaximumSize(new Dimension(300, 20));
                        panel.add(label);
                        panel.add(txtfield[i++]);
                        //i++;

                    }
                    else if(type.equals("submit"))
                    {
                        JButton submit_button=new JButton(each_child.attr("value"));
                        submit_button.addActionListener(new ActionListener() {

                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    throw new UnsupportedOperationException("Not supported yet.");
                                }
                            });

                        panel.add(submit_button);
                    }
                    else if(type.equals("checkbox"))
                    {
                        JCheckBox chkbox=new JCheckBox(st.nextToken());
                        panel.add(chkbox);
                    }

                }
            }        
        } 
    }
    
    void Clear_Container()
    {
        panel.removeAll();
    }


    
}
